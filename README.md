# 3D Waveform Demo

A simple OpenGL program that demonstrates how to render a 3D waveform. Built for Windows only, but there's no reason it couldn't be compiled under Linux or another operating system (although anything that uses GLES, like the Raspberry Pi, would require some minor modifications).

## Running

Both the debug and release executables are included in the root directory. Use the the mouse (click-and-drag) to rotate, and the mouse wheel to zoom.

## Compiling

Executable is already included, but if you want to make modifications and build it, you'll need Code::Blocks. If you don't have it already, you can download Code::Blocks from [here](http://codeblocks.org/downloads/26).

## License

No license. For cryin' out loud. People should really stop with that license nonsense---it's too confusing.

## Contact

Feel free to contact Geoff at geoff.nagy@gmail.com if you have any questions.