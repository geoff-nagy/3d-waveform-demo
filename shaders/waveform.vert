#version 150

uniform mat4 u_Projection;		// camera projection (e.g., orthographic, perspective)
uniform mat4 u_Model;			// how this vertex should be transformed
uniform mat4 u_View;			// position and orientation of camera

uniform vec4 u_Color1;			// color of vertex when height of vertex is u_Height1
uniform vec4 u_Color2;			// color of vertex when height of vertex is u_Height2
uniform float u_Height1;			// at this height, color of vertex is u_Color1
uniform float u_Height2;			// at this height, color of vertex is u_Color2

in vec2 a_Vertex;				// incoming vertex position in 2D
in float a_Height;				// incoming vertex height, kept separate for simplicity of changing the waveform shape

out vec4 v_Color;				// outgoing color, being passed to fragment shader after interpolation

void main()
{
	// compute a vertex position based on the incoming coordinates, and put it into a 4D vector
	// so we can multiply with our 4x4 transformation matrix
	vec4 vertex = vec4(a_Vertex.x, a_Height, a_Vertex.y, 1.0);

	// compute the resulting color based on the height of the vertex
	float colorFactor = clamp((a_Height - u_Height1) / (u_Height2 - u_Height1), 0.0, 1.0);
	v_Color = mix(u_Color1, u_Color2, colorFactor);

	// matrix transformations are always evaluated in reverse order, so the "vertex" vector
	// multiplication here is done first, which is what we want, and so on...
	gl_Position = u_Projection * u_View * u_Model * vertex;
}
