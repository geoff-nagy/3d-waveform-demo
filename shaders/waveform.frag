#version 150

in vec4 v_Color;			// incoming color, computed by interpolating the v_Colors from surrounding vertices

out vec4 f_FragColor;		// GLSL 1.5 requires a single output vec4, to be used as the fragment color

void main()
{
	// all we need to do is assign the outgoing fragment color to the interpolated vertex color value
	f_FragColor = v_Color;
}
