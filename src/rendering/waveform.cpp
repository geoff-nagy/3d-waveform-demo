#include "rendering/waveform.h"

#include "util/shader.h"

#include "gl/glew.h"
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
using namespace glm;

Waveform::Waveform(int numTiles, float tileSize)
{
	setupVBOs(numTiles, tileSize);
	setupShader();
}

Waveform::~Waveform()
{
	delete shader;
}

void Waveform::setupVBOs(int numVertices, float tileSize)
{
	const int NUM_VERTICES = numVertices * numVertices;					// number of vertices we need to render the complete waveform object
	const int NUM_INDICES = (numVertices - 1) * (numVertices - 1) * 6;	// each tile is made up of 2 triangles, each with 3 vertices

	vec2 *vertices = new vec2[NUM_VERTICES];			// temporary array for sending 2D vertex positions to the GPU
	vec2 *vertexPtr = vertices;							// iterating via pointer is faster than using array subscripts
	int i, j;

	GLuint *indices = new GLuint[NUM_INDICES];			// temporary array for sending vertex indices to the GPU
	GLuint *indexPtr = indices;							// iterating via pointer is faster than using array subscripts

	// save waveform characteristics in case we need them later
	this -> numVertices = numVertices;
	this -> tileSize = tileSize;
	numIndices = NUM_INDICES;

	// set the vertex positions; we only do this in 2D since we want to be able to easily set the waveform heights independently
	// via a separate set of vertex attributes
	for(i = -numVertices / 2; i < numVertices / 2; i ++)
	{
		for(j = -numVertices / 2; j < numVertices / 2; j ++)
		{
			*vertexPtr++ = vec2(j * tileSize, -(i * tileSize));
		}
	}

	// compute the indices used to render the waveform;
	// each waveform tile is made up of 2 triangles, each using 3 of the vertices from above
	for(i = 0; i < numVertices - 1; i ++)
	{
		for(j = 0; j < numVertices - 1; j ++)
		{
			// top left triangle
			*indexPtr++ = (i * numVertices) + j;				// bottom left corner
			*indexPtr++ = ((i + 1) * numVertices) + j;			// top left corner
			*indexPtr++ = ((i + 1) * numVertices) + j + 1;		// top right corner

			// bottom right triangle
			*indexPtr++ = (i * numVertices) + j;				// bottom left corner
			*indexPtr++ = ((i + 1) * numVertices) + j + 1;		// top right corner
			*indexPtr++ = (i * numVertices) + j + 1;			// bottom right corner
		}
	}

	// create our vertex array object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(4, vbos);

	// now send our waveform vertices to sit happily in the GPU for later
	glBindBuffer(GL_ARRAY_BUFFER, vbos[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * NUM_VERTICES, vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);					// enable attribute #0
	glVertexAttribPointer(0,						// this call pertains to attribute #0
						  2,						// two elements per attribute
						  GL_FLOAT,					// data type is GLfloat
						  GL_FALSE,					// do not normalize the attributes
						  0,						// no stride
						  (GLvoid*)0);				// no offset

	// allocate some space for the vertex heights of our waveform; we will fill these in later
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * NUM_VERTICES, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(1);					// enable attribute #1
	glVertexAttribPointer(1,						// this call pertains to attribute #1
						  1,						// one element per attribute
						  GL_FLOAT,					// data type is GLfloat
						  GL_FALSE,					// do not normalize the attributes
						  0,						// no stride
						  (GLvoid*)0);				// no offset

	// our indices define how our waveform should be constructed using the vertices defined above;
	// send our indies to the GPU; with them, we can render the entire waveform without wasting or defining duplicate vertices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos[2]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * NUM_INDICES, indices, GL_STATIC_DRAW);

	// free up the memory we've used
	delete[] indices;
	delete[] vertices;
}

void Waveform::setupShader()
{
	shader = new Shader("shaders/waveform.vert", "shaders/waveform.frag");
	shader -> bindAttrib("a_Vertex", 0);
	shader -> bindAttrib("a_Height", 1);
	shader -> link();
	shader -> bind();
	shader -> uniformVec4("u_Color1", vec4(0.4, 0.1, 0.4, 0.2));			// see waveform.vert about what these 4 uniforms do
	shader -> uniformVec4("u_Color2", vec4(0.2, 0.4, 0.8, 0.8));
	shader -> uniform1f("u_Height1", -1.0);
	shader -> uniform1f("u_Height2", 1.0);
	shader -> unbind();
}

void Waveform::setHeights(float *heights)
{
	// bring in our vertex buffer state, activate the buffer used for waveform height, and pass in the data we received (to the GPU)
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * numVertices * numVertices, heights);
}

void Waveform::render(mat4 &projection, mat4 &model, mat4 &view)
{
	// activate our shader and pass in the viewing params
	shader -> bind();
	shader -> uniformMatrix4fv("u_Projection", 1, value_ptr(projection));
	shader -> uniformMatrix4fv("u_Model", 1, value_ptr(model));
	shader -> uniformMatrix4fv("u_View", 1, value_ptr(view));

	// bring in our GL vertex attribute state
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, NULL);
}
