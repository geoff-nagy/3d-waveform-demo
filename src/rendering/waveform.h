#pragma once

#include "gl/glew.h"
#include "glm/glm.hpp"

class Shader;

class Waveform
{
private:
	GLuint vao;				// identifier for the GL object that contains our vertex attribute states
	GLuint vbos[3];			// identifiers for the GL vertex buffers we use to render the waveform (2D vertex, height, and indices)

	Shader *shader;			// since we're using GL 3.2, we must provide our own shaders since fixed-functionality is disabled

	int numVertices;		// the number of tiles wide and long our waveform should be
	float tileSize;			// how wide and long each tile should be, in arbitrary 3D coordinates
	int numIndices;			// the number of indices that will be rendered with a render() call; see setupVBOs() for more info

	void setupVBOs(int numTiles, float tileSize);		// set aside and configure some of the memory we'll need to pass into the GPU
	void setupShader();									// load up and compile our shader object

public:
	// numVertices: number of vertices wide and long the waveform will be (numVertices x numVertices)
	// tileSize: how wide and long each tile of the waveform is, in arbitrary 3D coordinates
	Waveform(int numVertices, float tileSize);
	~Waveform();

	// sets the vertex heights in arbitrary 3D coordinates; assumes that the size of this array is large enough
	// and contains heights for all of the vertices in the waveform
	void setHeights(float *heights);

	// renders the waveform using the given viewing parameters
	// projection: the type of camera projection used (e.g., orthographic, perspective)
	// model: defines how the waveform should be transformed
	// view: the position and orientation of the camera
	void render(glm::mat4 &projection, glm::mat4 &model, glm::mat4 &view);
};
